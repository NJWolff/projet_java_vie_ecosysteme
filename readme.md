﻿# README VIE D'UN ECOSYSTEME 
@auteurs
Constance Barban 
Nathan Wolff
Quentin Le Port
---
## Installation de l'application et lancement
**1.** installer le [JRE](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) (java runtime environment) fourni avec l'application et un [IDE](https://www.eclipse.org/downloads/)
**2.** extraire l'archive dans votre eclipse-workspace
 ![enter image description here](https://lh3.googleusercontent.com/tt5TkDRLkaO5yjR6fHWIYZ_BGwxtqZf2ZZ9UxP9uO0ZVoogWBlYIX4abFQWxabOdyzH0SM77ZNM)
 
* importer le projet dans JAVA
 
 ![enter image description here](https://lh3.googleusercontent.com/WW-SjLJq3_9b3lSqMHuSFnajJlK6LlAHOH6nwWCMeGw7ajDK4BPxHxZxo9f-ciftiVldh2wGoYY)![enter image description here](https://lh3.googleusercontent.com/FK73pu34g5gApVw_a2bqS6XeDtaqUNczMhV9ej5dCfplI8eAlLsY-UJqeGFVWzjYaK_Fe5cABtg)
**3.** installer postgreSQL grâce à l'exécutable fourni dans le rendu (pgadmin 4 sera installé également) 
	![enter image description here](https://lh3.googleusercontent.com/jTNjtHxYj_0xrEUv3OCEWQu89DfPW1nAIrhuRbme2zKONwiKlyXSj8Bg92zvXC0WTI3xIu-qoao)
> lancer l'exécutable
> cocher à minima les cases **PostgreSQL Server** et **pgadmin 4**
> installer postgreSQL sur le port 5432
	
![enter image description here](https://lh3.googleusercontent.com/l7CZE_-hfR_H9Sep9PFiEdV53TaqRs1m4SXu4AhX7Hqn01hdMkUm0--eqa4Mk2W6htA04vFesVA)
**4.** ouvrir le projet dans Eclipse; si un autre JRE est déjà sur votre machine des erreurs sont possibles **JavaBuildPath** & **il faut s'assurer que l'encodage est UTF-8**
*gestion de l'encodage :*
	 - Project
	 - Properties 
	 - Ressources 
	 - Text encoding file : sélectionner Other puis UTF-8, Apply and Close
*gestion du Build Path :*
	- Project 
	- Properties 
	- Java Build Path 
	- Libraries 
	- Sélectionner le JRE : Edit --> Alternate JRE sélectionner le JRE installé
**5.** importer le driver postgresql
![enter image description here](https://lh3.googleusercontent.com/7FrkOjgscH2hI1IRZrnvyKnQJVc8ysf1E8pLWdbrEsC40mz_QaXdSfJKt7tUEWh4Kx0i-NbvRKo)![enter image description here](https://lh3.googleusercontent.com/OnTa-Nk_2a_XerptweZRQFsoNMDBLfSlL2RzPT5Y7AGcdqsqDu8qCJ82LQS7KW4UQKOIFfdpvq4)![enter image description here](https://lh3.googleusercontent.com/IMOuC1zS-1mWxntHTZndsqlGYOW_b6KfrrlCTSPiJ_KDr9M5asomr4tHf6VxJyETNQnYbArpsHU)![enter image description here](https://lh3.googleusercontent.com/eXUK8pKks1JaePr3aRjVfMgf2vtu8NA5X0OOyaFVOT5b7yVeb11d6uOPYqgQzTg_i0avmap7gEY)
**6.** Lancer pgadmin 4 fourni avec l'application 
![enter image description here](https://lh3.googleusercontent.com/HgyVDdhIU85hrFAESB7Nl2yVj7W9VkjphDLDUpNgwTHcf66EBno8Q5bExO3evBFJpCb6tXbqwMM)

**7.** Pour récupérer la base de données : créer une base de données vide pour réaliser le backup (voir ci-dessous). Utiliser le fichier faunevide.backup pour récupérer les tables.
IMPORTANT: veillez à bien respecter la syntaxe pour le nom de la base de données.
![enter image description here](https://lh3.googleusercontent.com/4ePU0gR4q4D7_ba30zCY5bDfRIR_M5GixjOKuHSmUhXIeAzjORUVoQuWIm-iBIW-st5r4haG9lU)![enter image description here](https://lh3.googleusercontent.com/oJcIAsiLJe7sfawvLoMyN4D8P9JkTojUDqHtGruRYrjb9UhXqHwV-g0eZHfWG76EQV3MQIjTByk)
![enter image description here](https://lh3.googleusercontent.com/32Aul1krJ2yuRE2I1_vlN40JEejgPm0EX_KFJFrS3WwJCGhh1czhstlBzgYrdryxbLX6r3su1t4)
![enter image description here](https://lh3.googleusercontent.com/Jbf6opfceXZa5jS45Zjc4HsXapKIs8MgI5YUhBfLhehxXzend5ZB2cJD6zbgy22WRetKdC8VssQ)
![enter image description here](https://lh3.googleusercontent.com/Wyy1BKxOMT2emE2mKzUfbBaksZfwO2_fMkJaEckEove7WCjqioX55IezSz--m5G6A328nAIsI8E)

## Possibilités utilisateur
Vous pourrez trouver une description de l'application dans le bouton Instructions du Menu qui résume les principales caractéristiques de l'application, des informations sur l'affichage graphique et quelques informations complémentaires sur la façon dont sont initialisés les animaux.
![enter image description here](https://lh3.googleusercontent.com/xS5JRRorwCytycaJ2l6X9Rf8_J-NbVThYAmXyvjAtqIfBB-1G85BktdXZHNyNwQjtvFvNoAKBEo)
### Quelques  requêtes SQL
Pour manipuler les éléments de la base de données et récupérer des informations complémentaires lors de la simulation. Nous rappelons que la base de données est actualisée à chaque tour de jeu et que pour y visualiser les résultats il est nécessaire de répéter les requêtes. 
(exemples non exhaustifs)
```
SELECT * 
FROM faune
```
récupère  l'ensemble de la table faune 
```
SELECT * 
FROM case_carte
```
récupère l'ensemble des cases (permet de vérifier si la fonction de mise à jour de la base de données a bien ajouté l'ensemble des cases qui composent la grille) 
```
SELECT id_animal,espece, numligne, numcolonne
FROM faune,case_carte
WHERE position = id_case
```
permet de connaître dans la base de données la position de l'animal et son espèce 

